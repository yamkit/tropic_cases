require './lib/sheet_reader'
require './lib/riak_client'
require './lib/worksheet2json'
require './lib/ticket_format'

class InterfaceClient

  SHEET_PATH = '/opt/data/data.xls'

  def self.use
    data = SheetReader.new(SHEET_PATH).read
    client = RiakClient.new.source
    bucket = client.bucket('tropic_tickets')

    data.each do | row |
      object = bucket.get_or_new(row['id'])
      object.raw_data = row.to_json
      object.content_type = 'application/json'
      object.store
      print "."
    end

    data.size
  end
end

size = InterfaceClient.use
p "import #{size} rows"
