class Worksheet2json

  def convert worksheet, column_names=[]
    result = []
    header = worksheet.row 0
    header.compact!

    if !column_names.empty?
      header = TicketFormat.translate header
    end

    # worksheet.merge_cells 1, 2, worksheet.row_count-1, 3

    worksheet.each 1 do |row|
      stringify_cells!(row)

      row[2] = [ row[2], "(#{row[3]})" ].join(' ')

      row.delete_at(3)
      # MUST remove format at column 3
      # make sure cells are compatible with formats
      row.formats.delete_at(3)

      result << Hash[header.zip(row)]
    end
    result
  end

  private
  def stringify_cells! cells
    cells = Array(cells)
    # 为什么不能用map
    cells.each_with_index do |cell, index|
      cells[index] = stringify_cell(cell)
    end
  end

  def stringify_cell cell
    if cell.instance_of?(DateTime)
      return cell.strftime('%F %H:%M')
    elsif cell.instance_of?(Float)
      return cell.to_i.to_s
    elsif cell.instance_of?(Spreadsheet::Formula)
      return cell.value
    else
      return cell
    end
  end
end
