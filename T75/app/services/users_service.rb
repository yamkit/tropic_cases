class UsersService

  attr_reader :posts_service
  attr_reader :organizations_service

  def initialize
    @posts_service = PostsService.new
    @organizations_service = OrganizationsService.new
  end

  def find identifier
    users_repository.select do |user|
      user.id == identifier
    end.first
  end

  def humanize user
    humanized_user = {}
    humanized_user[:id] = user.id
    humanized_user[:name] = user.name
    roles = user.organization_ids.map do |organ_id|
      organization = organizations_service.find organ_id
      posts = posts_service.query_by_organ_id organization.id
      # binding.pry
      {organ: organization.name, assign: posts.map(&:name)}
    end
    # roles = user[:organs].map do |organ_id|
    #   organization = organizations_service.find organ[:id]
    #   assign_names = organ[:posts].map do |post_id|
    #     posts_service.find(post_id)[:name]
    #   end
    #   {organ: organization[:name], assign: assign_names}
    # end
    humanized_user[:roles] = roles
    humanized_user
  end

  private
  def users_repository
    users = []
    u1 = User.new
    u1.id = 1
    u1.name = '张三'
    u1.add_organization_id [1, 2]
    users << u1
    u2 = User.new
    u2.id = 2
    u2.name = '李四'
    u2.add_organization_id [1, 3]
    users << u2
    # [
    #   {id: 1, name: "张三", organs: [ {id: 1, posts: [1, 2]}, {id: 2, posts: [1, 2]}]},
    #   {id: 2, name: "李四", organs: [ {id: 1, posts: [1, 2]}, {id: 3, posts: [1, 2]}]}
    # ]
    users
  end
end
