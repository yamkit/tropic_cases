class UsersController < Sinatra::Base

  get '/:id' do
    @users_service = UsersService.new
    content_type :json
    @users_service.humanize(@users_service.find(params[:id].to_i)).to_json
  end
end
