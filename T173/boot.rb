require File.expand_path('../config/application', __FILE__)

LOGGER.info "current env   ===> #{ENV['RUNNING_ENV']}"
LOGGER.info "endpoint uri  ===> #{ENV['ENDPOINT_URI']}"
LOGGER.info "sync period   ===> #{ENV['SYNC_PERIOD']}"

SyncScheduler.new(ENV['SYNC_PERIOD']).execute
