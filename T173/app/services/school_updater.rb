class SchoolUpdater

  attr_reader :data_source

  def initialize school_source
    @data_source = school_source
  end

  def execute
    update_school
    SyncActivity.create start_date: data_source.sync_start_time,
                        end_date: data_source.sync_end_time,
                        added_class_numbers: data_source.classes.count,
                        added_student_numbers: data_source.students.count

  end

  private
  def update_school
    data_source.classes.map do |clazz|
      saved_clazz = Clazz.create class_id: clazz['classId'],
                                 class_name: clazz['className'],
                                 semester: clazz['semester'],
                                 start_date: clazz['startDate'],
                                 end_date: clazz['endDate'],
                                 class_teacher: clazz['classTeacher'],
                                 class_room: clazz['classRoom'],
                                 numbers: clazz['numbers']
      clazz['students'].each do |student|
        saved_clazz.students.create student_id: student['studentId'],
                                    student_name: student['studentName'],
                                    company: student['company'],
                                    phone_number: student['phoneNumber'],
                                    id_number: student['IDNumber'],
                                    erp_number: student['ERPNumber']
      end
    end
  end
end
