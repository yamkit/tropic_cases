require 'httparty'

class SchoolSource
  include HTTParty
  base_uri ENV['ENDPOINT_URI']

  attr_reader :sync_start_time

  def initialize(sync_start_time=nil)
    @sync_start_time = sync_start_time
    if @sync_start_time
      @options = { query: {syncTimeStamp: @sync_start_time}, format: :json}
    else
      @options = {}
    end
  end

  def sync_end_time
    data_source['syncTimeStamp']
  end

  def students
    @students ||= classes.map{|clazz| clazz['students']}.flatten
  end

  def classes
    data_source['result']
  end

  private
  def data_source
    @data_source ||= JSON.parse(self.class.get("/WCTMS/api", @options).body)
  end
end
