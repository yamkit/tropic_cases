class Student
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :clazz

  field :student_id, type: String
  field :student_name, type: String
  field :company, type: String
  field :phone_number, type: String
  field :id_number, type: String
  field :erp_number, type: String

  # 签到日期
  field :sign_time, type: DateTime
  # 电子签名
  field :signature, type: String
  # 签到日期备份
  field :backup_sign_time, type: DateTime
  # 电子签名备份
  field :backup_signature, type: String

end
