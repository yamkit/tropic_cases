class AlarmCategoryService
  include HTTParty

  digest_auth ENV['IMCRS_USER'], ENV['IMCRS_PASSWD']
  base_uri ENV['IMCRS_BASE_URI']

  attr_reader :categories

  def query
    self.class.get("/fault/alarmCategory", headers: {'Accept' => 'application/json'})['alarmCategory']
  end

  def query_with_alarms(options = {})
    alarm_service = AlarmService.new
    query.map do |category|
      alarms_count = alarm_service.query(options.merge({alarmCategory: category['id'], total: true })).to_i
      result = { base_class: category['baseClass'], base_desc: category['baseDesc'], sub_class: category['subClass'], sub_desc: category['subDesc'], count: alarms_count, alarms: [] }
      alarms_query_limit = alarms_count > ENV['ALARMS_QUERY_LIMIT'].to_i ? ENV['ALARMS_QUERY_LIMIT'].to_i : alarms_count
      result[:alarms] = alarm_service.query({ alarmCategory: category['id'], size: alarms_query_limit })
      # (0...alarms_count).each_slice(200).to_a.map do |range|
      #   result[:alarms].concat  alarm_service.query({ alarmCategory: category['id'], startAlarmTime: start_time, endAlarmTime: end_time, start: range.first, size: 100 })
      # end
      result
    end
  end
end
