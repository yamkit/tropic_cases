require 'pp'
require 'pry'
require 'grape'
require 'dotenv'
require 'httparty'
Dotenv.load(File.expand_path("../config/settings.env",  __FILE__))

APP_ROOT = Pathname.new(File.expand_path('../', __FILE__))

Dir[APP_ROOT.join('app', 'api', '*.rb')].each { |file| require file }
Dir[APP_ROOT.join('lib', 'services', '*.rb')].each { |file| require file }


run NetworkAlarms::API
